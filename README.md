# Theip.me

It's a service that will give you back your public ip address plus all the
headers involved in the connection. The answer depends on the content supported
or accepted by the client who makes the request. For example

```sh
curl -H "Accept: application/json" https://theip.me
```

It will return a json answer

```json
{
  "accept": "application/json,text/html",
  "connection": "close",
  "host": "theip.me",
  "ip": "51.15.238.112",
  "user-agent": "curl/7.64.0",
  "x-forwarded-for": "51.15.238.112",
  "x-forwarded-port": "443",
  "x-forwarded-proto": "https",
  "x-forwarded-ssl": "on",
  "x-real-ip": "51.15.238.112"
}
```

If you don't support json or html, you will get only your public ip as simple text

```sh
$ curl https://theip.me
51.15.238.112
```

Last but not least, if you hit the url using a browser, you will get something like https://theip.me :)

# Docker image

You can pull the docker image generated by the pipeline as following

```sh
docker pull registry.gitlab.com/agustinhenze/theip-me:latest
```

If you want to build it locally

```sh
$ docker build -f docker/Dockerfile -t theip-me .
```
