use actix_web::{middleware, App, HttpServer};

use tera::Tera;

use theip_me;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    HttpServer::new(|| {
        let tera = Tera::new(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*")).unwrap();

        App::new()
            .data(tera)
            .wrap(middleware::Logger::default())
            .service(theip_me::get_ip)
    })
    .bind("0.0.0.0:8008")?
    .workers(8)
    .run()
    .await
}
