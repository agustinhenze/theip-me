use actix_web::http::header;

use actix_web::{get, web, HttpRequest, HttpResponse, Responder};
use log::debug;
use std::collections::HashMap;

const X_FORWARDED_FOR: &[u8] = b"x-forwarded-for";

pub(crate) fn evaluate_forwarded_header(val: &str) -> Option<&str> {
    let mut realip_remote_addr: Option<&str> = None;
    debug!("The header is! {}", val);
    for pair in val.split(';') {
        for el in pair.split(',') {
            let mut items = el.trim().splitn(2, '=');
            if let Some(name) = items.next() {
                if let Some(val) = items.next() {
                    match &name.to_lowercase() as &str {
                        "for" => {
                            //does this still makes sense?
                            if realip_remote_addr.is_none() {
                                realip_remote_addr = Some(val.trim());
                            }
                        }
                        _ => debug!("Unexpected header format: {:?}", val),
                    }
                }
            }
        }
    }

    realip_remote_addr
}

pub fn get_ip_from_req(req: &HttpRequest) -> Option<String> {
    let mut realip_remote_addr = None;

    // load forwarded header
    for hdr in req.headers().get_all(&header::FORWARDED) {
        let hdr = hdr.to_str();
        if let Ok(val) = hdr {
            if let Some(addr) = evaluate_forwarded_header(val) {
                if realip_remote_addr.is_none() {
                    realip_remote_addr = Some(addr)
                } else {
                    debug!(
                        "Realip already found, but there are more \"forwarded\" headers: {:?}",
                        val
                    );
                }
            }
        }
    }
    // get remote_addraddr from socketaddr
    let remote_addr = req.peer_addr().map(|addr| format!("{}", addr.ip()));

    if realip_remote_addr.is_none() {
        if let Some(h) = req
            .headers()
            .get(&header::HeaderName::from_lowercase(X_FORWARDED_FOR).unwrap())
        {
            debug!("Evaluating header of type: {:?}", X_FORWARDED_FOR);
            if let Ok(h) = h.to_str() {
                debug!("The header is: {}", h);
                realip_remote_addr = h.split(',').next().map(|v| v.trim());
            }
        }
    }

    if realip_remote_addr.is_some() {
        Some(String::from(realip_remote_addr.unwrap()))
    } else {
        Some(String::from(remote_addr.unwrap()))
    }
}

#[get("/")]
pub async fn get_ip(tmpl: web::Data<tera::Tera>, req: HttpRequest) -> impl Responder {
    let mut ctx = tera::Context::new();
    let mut data = HashMap::new();

    for (k, v) in req.headers().iter() {
        data.insert(k.as_str(), v.to_str().unwrap());
    }
    let addr = get_ip_from_req(&req).unwrap();
    data.insert("ip", &addr);
    ctx.insert("data", &data);

    if let Some(accept) = req.headers().get("accept") {
        if accept.to_str().unwrap().contains("json") {
            debug!("Accept json");
            let body = tmpl.render("ip.json", &ctx).unwrap();
            return HttpResponse::Ok().content_type("text/json").body(body);
        } else if accept.to_str().unwrap().contains("html") {
            debug!("Accept html");
            let body = tmpl.render("ip.html", &ctx).unwrap();
            return HttpResponse::Ok().content_type("text/html").body(body);
        }
    }
    debug!("Accept not json nor html");

    let body = tmpl.render("ip_raw.html", &ctx).unwrap();
    HttpResponse::Ok().content_type("text/html").body(body)
}

#[cfg(test)]
mod tests {
    use super::evaluate_forwarded_header;

    #[test]
    fn test_evaluate_header() {
        let test_header = "Forwarded: for=192.0.2.43, for=198.51.100.17";
        if let Some(ip) = evaluate_forwarded_header(&test_header) {
            assert_eq!(ip, "198.51.100.17");
        } else {
            panic!("ip not found!")
        }
    }
}
